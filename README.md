Example of using gitlab continuous integration and deployment CI/CD
https://docs.gitlab.com/ee/ci/
bwa Singularity container
Bionformatics package bwa
BWA is a software package for mapping DNA sequences against a large reference genome, such as the human genome. It consists of three algorithms: BWA-backtrack, BWA-SW and BWA-MEM.
https://github.com/lh3/bwa
bwa Version: 0.7.17
Create a folder with these 3 files:
.gitlab-ci.yml
README.md
bwa_v0.7.17.def
run:
git init
git remote rm origin
git remote add origin git@forgemia.inra.fr:/bwa.git
git add .
git commit -m "Initial commit"
git remote -v
git push -u origin master
Singularity container based on the recipe: bwa_v0.7.17.def
Package installation using Miniconda3 V4.7.12
image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:
singularity pull bwa_v0.7.17.sif oras://registry.forgemia.inra.fr//bwa/bwa:latest
Or you can download directly the singularity-image: artefacts.zip
